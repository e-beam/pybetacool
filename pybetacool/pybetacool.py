""" 
Simple Python interface to Betacool. 
Wish list:
- It allows to parse a BLD file, edit it, re-write it 
- It allows to run betacool using the generated bld file 
- It allows to parse a few betacool output files

Authors: J. D. Andersson, D. Gamba - (CERN)
Jan 2020

"""

import re
from collections import OrderedDict
import subprocess
import numpy as np

class PyBetaCool:

    def __init__(self, betacoolExec='./Betacool', BLDfilename='./betacool.bld'):
        """ Initializes variables and parses the input file."""
        self.betacoolExec = betacoolExec
        self.BLDfilename = BLDfilename

        # Parse input file
        self.BLDContent = BLDContent(BLDfilename)

    def runBetacool(self, command):
        out = subprocess.Popen([self.betacoolExec, self.BLDfilename, command], 
           stdout=subprocess.PIPE, 
           stderr=subprocess.STDOUT)
        stdout,_ = out.communicate()
        if 'FINISH-' in stdout.decode('ascii'):
            return True
        else:
            print(stdout.decode('ascii'))
            return False

    def parseCurveFile(self, filename):
        print('TODO: simple version here...')
        data = np.genfromtxt(filename, delimiter="\t")
        return data

    def parseSurfaceFile(self, filename):
        print('TODO: simple version here...')
        data = np.genfromtxt(filename, delimiter="\t")
        return data

class BLDContent:
    """
    Content of a .BLD file used as input for BetaCool.
    All content is kept as a string, with exception of row numbers. 
    """

    def __init__(self, BLDfilename = None):
        self.rows = OrderedDict()

        if BLDfilename is not None:
            self.parseFile(BLDfilename)

    def getValue(self, rowIdx, valueIdx):
        """ Gets a value from the current BLD configuration

        Args:
          rowIdx: the row number in the BLD file.

          valueIdx: the value
            WARNING: note that in betacool the first value is named "1". Here is "0"

        Returns:
            A string corresponding to the present value in the configuration
        """
        return self[rowIdx][valueIdx].value

    def getValueDescription(self, rowIdx, valueIdx):
        """ Gets a value description from the current BLD configuration

        Args:
          rowIdx: the row number in the BLD file.

          valueIdx: the value
            WARNING: note that in betacool the first value is named "1". Here is "0"

        Returns:
            A string corresponding to the description of the requested value
        """
        return self[rowIdx][valueIdx].description

    def setValue(self, rowIdx, valueIdx, value):
        """ Sets a new value in the current BLD configuration

        Args:
          rowIdx: the row number in the BLD file.

          valueIdx: the value
            WARNING: note that in betacool the first value is named "1". Here is "0"

          value: value to be set
            WARNING: the value will always be set as a string executing "str(value)"
        """
        self[rowIdx][valueIdx].value = str(value)
 
    # implement [] operators....
    def __getitem__(self, key):
        return self.rows[key]
    def __setitem__(self, key, value):
        if isinstance(value, self.RowDefinition):
            self.rows[key] = value
        else:
            self.rows[key] = self.RowDefinition(key, '', value)

    def parseFile(self, BLDfilename):
        """ Parses the betacool input (.bld) file, and returns an OrderedDict object with its content
        
        Args:
            BLDfilename: the .bld file to parses
        """

        with open(BLDfilename) as f:
            lines = f.readlines()

        pattern = r'\[row=(\d+)\](.*)'
        matcher = re.compile(pattern)
        row_nbr = 0
        description = ''
        singleRawContentLines = []
        for line in lines:
            match = matcher.search(line)
            if match is not None:
                if singleRawContentLines:
                    # This content belongs to the previous row
                    self.rows[row_nbr] = self.RowDefinition(
                        row_nbr, description, singleRawContentLines)
                    
                # start a new row definition
                row_nbr = int(match.group(1))
                description = match.group(2).strip()
                singleRawContentLines.clear()
            elif row_nbr > 0:
                singleRawContentLines.append(line)
            else:
                print(f'Warning: line "{line}" does not seem to belong to any row...')
        if singleRawContentLines:
            # this is must be the last row
            self.rows[row_nbr] = self.RowDefinition(
                row_nbr, description, singleRawContentLines)


    def save(self, filename):
        """ Takes the current object and prints it to a file. """

        f = open(filename, 'w')
        f.write(str(self))
        f.close()

    def __str__(self):
       return "".join([f'{str(v)}\n' for v in self.rows.values()])
    __repr__ = __str__

    class RowDefinition:
        """
        Each 'Row' block gets a definition (RowDefinition), which in turn
        is made up of RowAttributes (one attribute per row in the 'Row' block)
        """

        def __init__(self, row_nbr, description, contentLines):
            self.row_nbr = row_nbr
            self.description = description
            self.content = []

            pattern = r'(.*)[=;](.*)'
            matcher = re.compile(pattern)
            for line in contentLines:
                #match = matcher.search(line)
                splittedLine = re.split('[=;]', line, 1)

                """
                if match is not None:
                    value = match.group(1)
                    description = match.group(2).strip()
                    self.content.append(self.RowAttribute(value, description))
                """
                if len(splittedLine) == 2:
                    self.content.append(self.RowAttribute(splittedLine[0], splittedLine[1].strip()))
                elif line == '\n':
                    continue
                else:
                    print(f'Warning: Unknown content in line -{line}-... It will be lost...')


        # implement [] operators....
        def __getitem__(self, key):
            return self.content[key]
        def __setitem__(self, key, value):
            if isinstance(value, self.RowAttribute):
                self.content[key] = value
            else:
                self.content[key].value = value


        def __str__(self):
            # need to trick last content row to use ";" instead of =
            return f'[row={self.row_nbr}] {self.description}\n' \
                + "".join([f'{str(k)}\n' for k in self.content[:-1]]) \
                + f'{self.content[-1].value}; {self.content[-1].description}\n'
        __repr__ = __str__

        class RowAttribute:
            def __init__(self, value, description):
                self.value = value
                self.description = description

            def __str__(self):
                return f'{self.value}= {self.description}'
            __repr__ = __str__

def test_run():
    obj = PyBetaCool(BLDfilename='./nica_tk.bld', betacoolExec='./Betacool_MacOSX')
    obj.BLDContent.save('./nica_tk.bld-testing')
    obj.runBetacool('/friction')
    return obj
