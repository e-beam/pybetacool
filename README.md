# pybetacool

First-order interface to betacool for cooling studies.

## How to Install

```sh
pip install https://gitlab.cern.ch/e-beam/pybetacool.git
```

You also need to have betacool application for your architecture.
See [betacool]: https://gitlab.cern.ch/e-beam/betacool

## Usage

Import:

```python
import pybetacool
obj = PyBetaCool(BLDfilename='./nica_tk.bld', betacoolExec='./Betacool_MacOSX')
# Edit one parameter (row=60, 5th value is "Transverse temperature, eV")
obj.BLDContent[60][4] = 0.1 
# Overwrites nica_tk.bld with new setting 
obj.BLDContent.save()
# Execute Betacool
obj.runBetacool('/friction')
# Parse some output file
data = obj.parseCurveFile('flong.cur')
```
