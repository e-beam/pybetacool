from setuptools import setup

# This call to setup() does all the work
setup(
    name="pybetacool",
    version="0.0.1",
    description="First-order interface to betacool",
    author="Davide Gamba",
    author_email="davide.gamba@cern.ch",
    url="https://gitlab.cern.ch/e-beam/pybetacool",
    license="MIT",
    packages=["pybetacool"],
    install_requires=["numpy"],
)

